<?php

$lang['dashboard_app_description'] = 'Додаток «Панель» надає повний огляд вашої системи. Його можна налаштувати – встановлені програми можуть містити віджети для інформаційної панелі, які ви можете додати до свого макета.';
$lang['dashboard_app_name'] = 'Панель';
$lang['dashboard_configure_now'] = 'Ви не налаштували власну інформаційну панель. Налаштуйте його зараз або виберіть значення за замовчуванням.';
$lang['dashboard_configure'] = 'Ви можете налаштувати віджети або використовувати конфігурацію за замовчуванням';
$lang['dashboard_controller_missing'] = 'Віджет інформаційної панелі відсутній або видалений';
$lang['dashboard_controller_not_available'] = 'Додаток, що надає цей віджет, видалено і більше не доступний. Використовуйте інструмент «Видалити», щоб видалити цей заповнювач.';
$lang['dashboard_invalid_layout'] = 'Макет недійсний.';
$lang['dashboard_not_available'] = 'Недоступний';
$lang['dashboard_number_of_cols'] = 'Стовпці - Ряд';
$lang['dashboard_placeholder'] = 'Placeholder';
$lang['dashboard_select_widget'] = 'Вибір віджета';
$lang['dashboard_setup_required'] = 'Потрібне налаштування';
$lang['dashboard_no_widget'] = 'Немає віджетів';
$lang['dashboard_restart'] = 'Перезавантажити';
