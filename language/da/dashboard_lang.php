<?php

$lang['dashboard_app_description'] = 'Dashboardet giver et godt overblik over dit system. Det kan tilpasses - apps de er installeret inholder et widget og det kan du tilføje til Dashboardet.';
$lang['dashboard_app_name'] = 'Dashboard';
$lang['dashboard_configure_now'] = 'Du har ikke konfigureret din brugerdefinerede Dashboard. Konfigurer det nu, eller vælge standard.';
$lang['dashboard_controller_missing'] = 'Dashboard Widget Manglende eller Afinstalleret';
$lang['dashboard_controller_not_available'] = 'Det widget har været un-installeret og er ikke længere tilgængelig. Brug "Slet" værktøj til at fjerne denne pladsholder.';
$lang['dashboard_invalid_layout'] = 'Layout er ugyldig.';
$lang['dashboard_not_available'] = 'Ikke tilgængelig';
$lang['dashboard_number_of_cols'] = 'Kolonner - Row';
$lang['dashboard_placeholder'] = 'Pladsholder';
$lang['dashboard_select_widget'] = 'Widget Selection';
$lang['dashboard_setup_required'] = 'Opsætning Påkrævet';
